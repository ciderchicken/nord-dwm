# dwm
my custom nord themed dwm fork

install it clone the repo `git clone https://github.com/strawberry-milkshake500/dwm/` 

`cd dwm-6.2` 

and as root run

`make clean install` 

if you want the icons in the tags to work install the awesome font package `https://archlinux.org/packages/community/any/ttf-font-awesome/`

This build of dwm is deprecated many bugs in this build are fixed in my other build of dwm, please use that one instead
